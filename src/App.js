import logo from './logo.svg';
import './App.css';
import TopNavbar from './Header/TopNavbar';
import Slider from './Slider/Slider';
import img1 from './images/img1.png';
import img2 from './images/img2.png';
import img3 from './images/img3.png';

function App() {

  let menus = [
    {href: '#home', title: 'Home'},
    {href: '#features', title: 'Features'},
    {href: '#pricing', title: 'Pricing'},
  ]

  let sliders = [
    {
        img : {src: img1, alt: 'Image 1'},
        title: 'Payer avec check',
        description: 'best description'
    },
    {
        img : {src: img2, alt: 'Image 2'},
        title: 'Payer sans check',
        description: 'All descrption'
    },
    {
        img : {src: img3, alt: 'Image3'},
        title: 'Livraison',
        description: 'A velo'
    }
]

  return (
    <div className="Apps">
      <TopNavbar title="E-commerce" bg="dark" variant="dark" menus={menus}/>
      <Slider sliders={sliders}></Slider>
    </div>
  );
}

export default App;

