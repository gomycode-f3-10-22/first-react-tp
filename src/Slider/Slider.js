import Carousel from 'react-bootstrap/Carousel';

const Slider = ({sliders}) => {
    
    return (
        <div>
        <Carousel>
            
        {
            sliders && sliders.map((item, key) => (
                <Carousel.Item key={key}> 
                    <img
                    className="d-block w-100"
                    src={item.img.src}
                    alt={item.img.alt}
                    />
                    <Carousel.Caption>
                    <h3>{item.title}</h3>
                    <p>{item.description}</p>
                    </Carousel.Caption>
                </Carousel.Item>
            ))
        }

        </Carousel>
        </div>
    );
}

export default Slider;
