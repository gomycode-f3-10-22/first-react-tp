import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const TopNavbar = ({title, bg, variant, menus}) => {

    return (
        <div>
            <>
            <Navbar bg={bg} variant={variant}>
                <Container>
                <Navbar.Brand href="#home">{title}</Navbar.Brand>
                <Nav className="me-auto">
                    {
                        menus && menus.map((item, key) => (<Nav.Link key={key} href={item.href}>{item.title}</Nav.Link>))
                    }
                </Nav>
                </Container>
            </Navbar>
            </>
        </div>
    );
}

export default TopNavbar;
